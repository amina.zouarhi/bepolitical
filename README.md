# BePolitical

Common repository regrouping all parts of the application

## Commands :

Back-end : launch from Java IDE
Back-end - Admin Interface : ng serve
Front-end - User Interface : ionic serve

## Access :

Back-end : Accessed on port 8080
Back-end - Admin Interface : Accessed on port 4200
Front-end - User Interface : Accessed on port 8100
API Documentation : Accessed via Swinger
