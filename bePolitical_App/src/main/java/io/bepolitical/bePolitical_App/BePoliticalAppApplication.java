package io.bepolitical.bePolitical_App;

import io.bepolitical.bePolitical_App.model.Question;
import io.bepolitical.bePolitical_App.model.User;
import io.bepolitical.bePolitical_App.repo.QuestionRepo;
import io.bepolitical.bePolitical_App.repo.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


import java.util.Arrays;

import static io.bepolitical.bePolitical_App.enumeration.LevelStatus.*;

@EnableSwagger2
@EnableWebMvc // Comment out
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class BePoliticalAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BePoliticalAppApplication.class, args);
	}

	@Bean
	CommandLineRunner run(QuestionRepo questionRepo) {
		return  args -> {
			questionRepo.save(new Question(null, "Quels sont les devoirs des citoyens français ?", "toutes les réponses sont correctes",
					"http://localhost:8080/question/image/server1.png","Respecter les lois","Faire preuve de civisme et respecter les droits des autres", "toutes les réponses sont correctes", LEVEL_1 ));
			questionRepo.save(new Question(null, "Que représente la date suivante : 1789", "le début de la révolution Française",
					"http://localhost:8080/question/image/server2.png","A un roman de Victor Hugo","le début de la révolution Française", " une bière célèbre", LEVEL_2 ));
			questionRepo.save(new Question(null, "Dans quelle république sommes-nous ?", " La 5ème république",
					"http://localhost:8080/question/image/server3.png","La république française","La 4ème république", "La 5ème république", LEVEL_1 ));
			questionRepo.save(new Question(null, "Lequel de ces trois hommes n’a pas été président de la République ?", "Victor Hugo",
					"http://localhost:8080/question/image/server3.png","Valéry Giscard d’Estaing ","François Mitterrand", "Victor Hugo", LEVEL_1 ));
			questionRepo.save(new Question(null, "A partir de quel âge a-t-on le droit de voter en France ", "18 ans ",
					"http://localhost:8080/question/image/server3.png","16 ans ","18 ans ", "16 ans ", LEVEL_1 ));
			questionRepo.save(new Question(null, "Que représente le 14 Juillet ?", " C’est l’anniversaire de la prise de la bastille",
					"http://localhost:8080/question/image/server3.png"," C’est la fin de la première guerre mondiale","C’est la fête nationale", " C’est l’anniversaire de la prise de la bastille", LEVEL_1 ));
			questionRepo.save(new Question(null, "Le conseil régional dans une région gère ", " Les crèches",
					"http://localhost:8080/question/image/server3.png"," Les écoles primaires ","Les lycées", "Tous", LEVEL_2 ));
			questionRepo.save(new Question(null, "Le Conseil général dans un département gère ", " Les collèges",
					"http://localhost:8080/question/image/server3.png","Les crèches","Les lycées", "Les écoles primaires", LEVEL_2 ));
			questionRepo.save(new Question(null, "Comment sont désignés les députés ? ", " ils sont élus par les citoyens",
					"http://localhost:8080/question/image/server3.png","Ils sont choisis par les ministres","Ils sont nommés par le président de la République", "Ils sont choisis par le président", LEVEL_2 ));
			questionRepo.save(new Question(null, "Pour combien de temps est élu le président de la République ? ", "5 ans",
					"http://localhost:8080/question/image/server3.png","3 ans","2 ans", "7 ans", LEVEL_2 ));
			questionRepo.save(new Question(null, "Quel est le pouvoir chargé de faire les lois?", "Pouvoir législatif",
					"http://localhost:8080/question/image/server3.png","Pouvoir exécutif","Pouvoir judiciaire", "Pouvoir de l'amour", LEVEL_3 ));
		};
	};

	@Bean
	CommandLineRunner runuser(UserRepository userRepository){
		return  args -> {
			userRepository.save(new User(null, "admin", "admin@admin.com", "admintest", "admin", "test", true, new int[] {3, 1, 2, 5, 4}));

		};
	};

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:8100", "http://Localhost:4200", "http://Localhost:9000"));
		corsConfiguration.setAllowedHeaders(Arrays.asList("Origin", "Access-Control-Allow-Origin", "Content-Type",
				"Accept", "Jwt-Token", "Authorization", "Origin, Accept", "X-Requested-With",
				"Access-Control-Request-Method", "Access-Control-Request-Headers"));
		corsConfiguration.setExposedHeaders(Arrays.asList("Origin", "Content-Type", "Accept", "Jwt-Token", "Authorization",
				"Access-Control-Allow-Origin", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials", "Filename"));
		corsConfiguration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
		return new CorsFilter(urlBasedCorsConfigurationSource);
	}


}
