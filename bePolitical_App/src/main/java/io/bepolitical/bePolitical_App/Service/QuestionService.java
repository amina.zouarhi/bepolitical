package io.bepolitical.bePolitical_App.Service;

import io.bepolitical.bePolitical_App.model.Question;

import java.util.Collection;

public interface QuestionService {
    Question create(Question question);                           // create a question and save it in the database
    Collection<Question> list(int limit);                         // list all the questions with a limit
    Question get(Long id);                                        // return a question by id
    Question update(Question question);                           // take a question with the updated information and save it to the database
    Boolean delete(Long id);                                      // delete question from database based on it's id
}