package io.bepolitical.bePolitical_App.Service.implementation;

import io.bepolitical.bePolitical_App.Service.QuestionService;
import io.bepolitical.bePolitical_App.model.Question;
import io.bepolitical.bePolitical_App.repo.QuestionRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Random;

import static java.lang.Boolean.TRUE;
import static org.springframework.data.domain.PageRequest.of;

@RequiredArgsConstructor
@Service
@Transactional
@Slf4j
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepo questionRepo;

    @Override
    public Question create(Question question) {
        log.info("saving new Question: {}", question.getQuestion());
        question.setImageUrl(setImageUrl());
        return questionRepo.save(question);
    }

    @Override
    public Collection<Question> list(int limit) {
        log.info("Fetching all the questions");                     // with a limit
        return questionRepo.findAll(of(0,limit)).toList();
    }

    @Override
    public Question get(Long id) {
        log.info("Fetching question by id: {}", id);
        return questionRepo.findById(id).get();
    }

    @Override
    public Question update(Question question) {
        log.info("updating question: {}", question.getQuestion());
        return questionRepo.save(question);
    }

    @Override
    public Boolean delete(Long id) {
        log.info("Deleting question by id: {}", id);
        questionRepo.deleteById(id);
        return TRUE;
    }

    private String setImageUrl() {
        String[] imageNames = { "server1.png", "server2.png", "server3.png", "server4.png" };
        return ServletUriComponentsBuilder.fromCurrentContextPath().path("/question/image/" + imageNames[new Random().nextInt(4)]).toUriString();
    }
}
