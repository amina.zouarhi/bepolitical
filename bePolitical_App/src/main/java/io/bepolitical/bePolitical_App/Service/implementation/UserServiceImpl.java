package io.bepolitical.bePolitical_App.Service.implementation;

import io.bepolitical.bePolitical_App.Service.UserService;
import io.bepolitical.bePolitical_App.model.User;
import io.bepolitical.bePolitical_App.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;


import static java.lang.Boolean.TRUE;
import static org.springframework.data.domain.PageRequest.of;

@RequiredArgsConstructor
@Service
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    @Override
    public User create(User user) {
        log.info("saving new User: {}", user.getUserName());
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        return userRepository.save(user);
    }

    @Override
    public Collection<User> list(int limit) {
        log.info("Fetching all the users");
        return userRepository.findAll(of(0,limit)).toList();
    }

    @Override
    public User get(Long id) {
        log.info("Fetching users by id: {}", id);
        return userRepository.findById(id).get();
    }

    @Override
    public User update(User user) {
        log.info("updating user: {}", user.getUserName());
        return userRepository.save(user);
    }

    @Override
    public Boolean delete(Long id) {
        log.info("Deleting user by id: {}", id);
        userRepository.deleteById(id);
        return TRUE;
    }

}
