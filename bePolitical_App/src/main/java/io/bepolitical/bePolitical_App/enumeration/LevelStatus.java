package io.bepolitical.bePolitical_App.enumeration;

public enum LevelStatus {
    LEVEL_1("LEVEL_1"),
    LEVEL_2("LEVEL_2"),
    LEVEL_3("LEVEL_3"),
    LEVEL_4("LEVEL_4"),
    LEVEL_5("LEVEL_5"),
    LEVEL_6("LEVEL_6"),
    LEVEL_7("LEVEL_7");

    private final String level_status;

    LevelStatus(String level_status) { this.level_status = level_status;}

    public  String getLevel_status() {return this.level_status;}

}
