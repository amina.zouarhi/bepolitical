package io.bepolitical.bePolitical_App.enumeration;

public enum Status {
    LEVEL_CLEARED("LEVEL_CLEARED"),
    LEVEL_NOT_CLEARED("LEVEL_NOT_CLEARED");

    private final String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }
}
