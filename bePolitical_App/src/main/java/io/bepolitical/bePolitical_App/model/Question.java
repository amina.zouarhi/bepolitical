package io.bepolitical.bePolitical_App.model;

import io.bepolitical.bePolitical_App.enumeration.LevelStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Question {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @Column(unique = true)
    @NotEmpty(message = "question cannot be empty or null")
    private String question;
    @NotEmpty(message = "answer cannot be empty or null")
    private String answer;
    private String imageUrl;
    private String option1;
    private String option2;
    private String option3;
    private LevelStatus levelStatus;
}
