package io.bepolitical.bePolitical_App.repo;

import io.bepolitical.bePolitical_App.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepo extends JpaRepository<Question, Long> {
    Question findByQuestion(String question);
}