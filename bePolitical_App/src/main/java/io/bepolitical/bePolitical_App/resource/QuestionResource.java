package io.bepolitical.bePolitical_App.resource;

import io.bepolitical.bePolitical_App.Service.implementation.QuestionServiceImpl;
import io.bepolitical.bePolitical_App.model.Question;
import io.bepolitical.bePolitical_App.model.Response;
import io.bepolitical.bePolitical_App.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.time.LocalDateTime.now;
import static java.util.Map.of;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

@RestController
@RequestMapping("/question")
@RequiredArgsConstructor
public class QuestionResource {
    private final QuestionServiceImpl questionService;

    @GetMapping("/list")
    public ResponseEntity<Response> getQuestions() {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("questions", questionService.list(30)))
                        .message("questions retrieved")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @PostMapping("/save")
    public ResponseEntity<Response> saveQuestion(@RequestBody @Valid Question question)  {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("question", questionService.create(question)))
                        .message("question created")
                        .status(CREATED)
                        .statusCode(CREATED.value()) //201
                        .build()
        );
    }

    @PutMapping("/update")
    public ResponseEntity<Response> updateUser(@RequestBody @Valid Question question)  {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("user", questionService.update(question)))
                        .message("question created")
                        .status(CREATED)
                        .statusCode(CREATED.value()) //201
                        .build()
        );
    }


    @GetMapping("/get/{id}")
    public ResponseEntity<Response> getQuestion(@PathVariable("id") Long id) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("question", questionService.get(id)))
                        .message("question retrieved")
                        .status(OK)
                        .statusCode(OK.value()) //200
                        .build()
        );
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Response> deleteQuestion(@PathVariable("id") Long id) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("deleted", questionService.delete(id)))
                        .message("Question deleted")
                        .status(OK)
                        .statusCode(OK.value()) //200
                        .build()
        );
    }

    @GetMapping(path = "/image/{fileName}", produces = IMAGE_PNG_VALUE)
    public byte[] getQuestionImage(@PathVariable("fileName") String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(System.getProperty ("user.home") + "/Downloads/images/" + fileName));
    }
}

