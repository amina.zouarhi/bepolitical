package io.bepolitical.bePolitical_App.resource;

import io.bepolitical.bePolitical_App.Service.implementation.UserServiceImpl;
import io.bepolitical.bePolitical_App.model.Response;
import io.bepolitical.bePolitical_App.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static java.time.LocalDateTime.now;
import static java.util.Map.of;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserResource {
    private final UserServiceImpl userService;

    @GetMapping("/list")
    public ResponseEntity<Response> getUsers() {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("users", userService.list(30)))
                        .message("users retrieved")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @PostMapping("/save")
    public ResponseEntity<Response> saveUser(@RequestBody @Valid User user)  {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("user", userService.create(user)))
                        .message("user created")
                        .status(CREATED)
                        .statusCode(CREATED.value()) //201
                        .build()
        );
    }

    @PutMapping("/update")
    public ResponseEntity<Response> updateUser(@RequestBody @Valid User user)  {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("user",userService.update(user)))
                        .message("user created")
                        .status(CREATED)
                        .statusCode(CREATED.value()) //201
                        .build()
        );
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Response> getUser(@PathVariable("id") Long id) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("user", userService.get(id)))
                        .message("user retrieved")
                        .status(OK)
                        .statusCode(OK.value()) //200
                        .build()
        );
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Response> deleteQuestion(@PathVariable("id") Long id) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("deleted", userService.delete(id)))
                        .message("User deleted")
                        .status(OK)
                        .statusCode(OK.value()) //200
                        .build()
        );
    }





}
