import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, startWith } from 'rxjs/operators';
import { DataState } from './enum/data-state.enum';
import { LevelStatus } from './enum/levelStatus.enum';
import { AppState } from './interface/app-state';
import { CustomResponse } from './interface/custom-response';
import { Question } from './interface/question';
import { NotificationService } from './service/notification.service';
import { QuestionService } from './service/question.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  appState$: Observable<AppState<CustomResponse>>;
  readonly DataState = DataState;
  readonly LevelStatus = LevelStatus;
  private dataSubject = new BehaviorSubject<CustomResponse>(null);

  private isLoading = new BehaviorSubject<boolean>(false);
  isLoading$ = this.isLoading.asObservable();

  constructor(private questionService: QuestionService, private notifier: NotificationService) { }


  ngOnInit(): void {
    this.appState$ = this.questionService.questions$
      .pipe(
        map(response => {
          this.notifier.onDefault(response.message);
          this.dataSubject.next(response);
          return { dataState: DataState.LOADED_STATE, appData: { ...response, data: { questions: response.data.questions.reverse() } } }
        }),
        startWith({ dataState: DataState.LOADING_STATE }),
        catchError((error: string) => {
          this.notifier.onError(error);
          return of({ dataState: DataState.ERROR_STATE, error })
        })

      );
  }

  saveQuestion(questionForm: NgForm): void {
    this.isLoading.next(true);
    this.appState$ = this.questionService.save$(questionForm.value as Question)
      .pipe(
        map(response => {
          this.dataSubject.next(
            {...response, data: { questions: [response.data.question, ...this.dataSubject.value.data.questions] } }
          );
          this.notifier.onDefault(response.message);
          document.getElementById('closeModal').click();
          this.isLoading.next(false);
          questionForm.resetForm({ levelStatus: this.LevelStatus.ALL}); 
          return { dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }
        }),
        startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
        catchError((error: string) => {
          this.isLoading.next(false);
          this.notifier.onError(error);
          return of({ dataState: DataState.ERROR_STATE, error });
        })
      );
}

deleteQuestion(question: Question): void {
  this.appState$ = this.questionService.delete$(question.id)
    .pipe(
      map(response => {
        this.dataSubject.next(
          { ...response, data: { questions: this.dataSubject.value.data.questions.filter(q => q.id !== question.id)} }
        );
        this.notifier.onDefault(response.message);
        return { dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }
      }),
      startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
      catchError((error: string) => {
        this.notifier.onError(error);
        return of({ dataState: DataState.ERROR_STATE, error });
      })
    );
}

  filterQuestions(levelStatus : LevelStatus): void {
    this.appState$ = this.questionService.filter$(levelStatus, this.dataSubject.value)
      .pipe(
        map(response => {
          this.notifier.onDefault(response.message);
          return { dataState: DataState.LOADED_STATE, appData: response };
        }),
        startWith({ dataState: DataState.LOADED_STATE, appData: this.dataSubject.value }),
        catchError((error: string) => {
          this.notifier.onError(error);
          return of({ dataState: DataState.ERROR_STATE, error });
        })
      );
  }

  printQuestions() : void {
    window.print();
  }

  switchLevel(levelStatus : LevelStatus) : string {
    switch (levelStatus) {
      case LevelStatus.LEVEL_1 : {
        return 'LEVEL 1'
      }
      case LevelStatus.LEVEL_2 : {
        return 'LEVEL 2'
      }
      case LevelStatus.LEVEL_3 : {
        return 'LEVEL 3'
      }
      case LevelStatus.LEVEL_4 : {
        return 'LEVEL 4'
      }
      case LevelStatus.LEVEL_5 : {
        return 'LEVEL 5'
      }
      default: {
        return 'LEVEL 1'
      }
    }
  };

  switchLevelColor(levelStatus : LevelStatus) : string {
    switch (levelStatus) {
      case LevelStatus.LEVEL_1 : {
        return 'badge-success'
      }
      case LevelStatus.LEVEL_2 : {
        return 'badge-info'
      }
      case LevelStatus.LEVEL_3 : {
        return 'badge-warning'
      }
      case LevelStatus.LEVEL_4 : {
        return 'badge-danger'
      }
      case LevelStatus.LEVEL_5 : {
        return 'badge-dark'
      }
      default: {
        return 'badge-success'
      }
    }
  };


}
