export enum LevelStatus {
  ALL = 'ALL', LEVEL_1 = 'LEVEL_1', LEVEL_2 = 'LEVEL_2', LEVEL_3 = 'LEVEL_3', LEVEL_4 = 'LEVEL_4', LEVEL_5 = 'LEVEL_5', LEVEL_6 = 'LEVEL_6', LEVEL_7 = 'LEVEL_7'
}