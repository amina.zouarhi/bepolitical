import { LevelStatus } from "../enum/levelStatus.enum";

export interface Question {
  id: number;
  question: string;
  answer: string;
  imageUrl: string;
  option1: string;
  option2: string;
  option3: string;
  levelStatus: LevelStatus;

}