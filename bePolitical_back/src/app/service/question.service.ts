import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscriber, throwError } from 'rxjs';
import {  tap, catchError } from 'rxjs/operators';
import { LevelStatus } from '../enum/levelStatus.enum';
import { CustomResponse } from '../interface/custom-response';
import { Question } from '../interface/question';

@Injectable({ providedIn: 'root' })

export class QuestionService {

  private readonly apiUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  // observables

  questions$ = <Observable<CustomResponse>> this.http.get<CustomResponse>(`${this.apiUrl}/question/list`)
  .pipe(
    tap(console.log),
    catchError(this.handleError)
  );

  save$ = (question: Question) => <Observable<CustomResponse>> this.http.post<CustomResponse>(`${this.apiUrl}/question/save`, question)
  .pipe(
    tap(console.log),
    catchError(this.handleError)
  );

  filter$ = (levelStatus: LevelStatus, response: CustomResponse) => <Observable<CustomResponse>> 
  new Observable<CustomResponse>(
    subscriber => {
      console.log(response);
      subscriber.next(
        levelStatus === LevelStatus.ALL ? { ...response, message: `Questions filtered by ${levelStatus} status`} :
        {
          ...response,
          message: response.data.questions
          .filter(question => question.levelStatus === levelStatus).length > 0 ? `Questions filtered by ${this.checkLevel(levelStatus)} status` : `No questions of ${levelStatus} found`,
          data: {questions: response.data.questions
            .filter(question => question.levelStatus === levelStatus)}
        }
      );
      subscriber.complete()
    }
  )
  .pipe(
    tap(console.log),
    catchError(this.handleError)  
  );

  checkLevel(levelStatus : LevelStatus) : string {
    switch (levelStatus) {
      case LevelStatus.LEVEL_1 : {
        return 'LEVEL 1'
      }
      case LevelStatus.LEVEL_2 : {
        return 'LEVEL 2'
      }
      case LevelStatus.LEVEL_3 : {
        return 'LEVEL 3'
      }
      case LevelStatus.LEVEL_4 : {
        return 'LEVEL 4'
      }
      case LevelStatus.LEVEL_5 : {
        return 'LEVEL 5'
      }
      default: {
        return 'LEVEL 1'
      }
    }
  };


  delete$ = (questionId: number) => <Observable<CustomResponse>> this.http.delete<CustomResponse>(`${this.apiUrl}/question/delete/${questionId}`)
  .pipe(
    tap(console.log),
    catchError(this.handleError)  
  );

  private handleError(error: HttpErrorResponse): Observable<never> {
    console.log(error);
    return throwError (`An error had occurred - Error code: ${error.status} `);
  }

}


  // tipical way
  // getQuestions(): Observable<CustomResponse> {
  //   return this.http.get<CustomResponse>(`http://localhost:8080/question/list`);
  // }