import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'autentification', pathMatch: 'full' },
  {
    path: 'start',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'autentification',
    loadChildren: () => import('./autentification/autentification.module').then( m => m.AutentificationPageModule)
  },
  {
    path: 'question/:level',
    loadChildren: () => import('./question/question.module').then( m => m.QuestionPageModule)
  },  {
    path: 'inscription',
    loadChildren: () => import('./inscription/inscription.module').then( m => m.InscriptionPageModule)
  }



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
