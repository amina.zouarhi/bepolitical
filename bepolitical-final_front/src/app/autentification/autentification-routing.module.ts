import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutentificationPage } from './autentification.page';

const routes: Routes = [
  {
    path: '',
    component: AutentificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AutentificationPageRoutingModule {}
