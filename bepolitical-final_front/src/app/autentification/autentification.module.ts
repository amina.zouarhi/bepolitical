import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AutentificationPageRoutingModule } from './autentification-routing.module';

import { AutentificationPage } from './autentification.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AutentificationPageRoutingModule
  ],
  declarations: [AutentificationPage]
})
export class AutentificationPageModule {}
