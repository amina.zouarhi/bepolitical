import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { async } from "q";
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-autentification',
  templateUrl: './autentification.page.html',
  styleUrls: ['./autentification.page.scss'],
})
export class AutentificationPage implements OnInit {

  userData: any = {};
  mail: string;
  password: string;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  async navTabs(){
    this.userData = this.userService.getUser(this.mail);
    if (this.userData === 'Cet utilisateur n\'existe pas!') {
      console.log(this.userData);
    }
    else {
      this.userService.setUser(this.userData);
      this.router.navigateByUrl('/start/tabs/tab1');

    }
  }

  createUser() {
    this.router.navigateByUrl('/inscription');
  }



}
