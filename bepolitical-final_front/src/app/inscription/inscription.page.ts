import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  id=1;
  name:string;
  lastname:string;
  username:string;
  mail:string;
  password:string;
  user:any;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  auth(){
    this.user = {
      id: this.id,
      userName: this.username,
      email: this.mail,
      password: this.password,
      name: this.name,
      lastName: this.lastname,
      active: true,
      score: [
          0,
          0,
          0,
          0,
          0
      ]
    };
    this.userService.addUser(this.user);
    this.router.navigateByUrl('/autentification');
  }

  quit() {
    this.router.navigateByUrl('/autentification');
  }
}
