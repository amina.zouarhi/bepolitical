import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CountdownEvent, CountdownComponent} from 'ngx-countdown';
import { LevelService } from '../service/level.service';

import { UserService } from '../service/user.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

  @ViewChild('timer') counter: CountdownComponent;


  currentLevel: number;
  questions: any;
  question: any = null;
  config: any;
  numberOfQuestions = 3;
  numberOfCorrectAnswers = 0;
  id=0;
  answerColors=[];
  passOrFail = 'fail';
  showResult = false;
  time = 5;
  nextButton = true;







  constructor(private router: Router, private userService: UserService, private levelService: LevelService) { }

  ngOnInit() {
    //this.id = this.userService.getDeviceId();
    setTimeout(() => {
      this.currentLevel = this.userService.getCurrentLevel();
      console.log('Question : Level = '+this.currentLevel);
      this.questions = this.levelService.getLevelQuestions(this.currentLevel);
      this.question = this.questions[this.id++];
      this.question.answers = this.shuffle([this.question.answer,this.question.option1,this.question.option2,this.question.option3]);
      console.log(' ---Question:this.question : '+this.question.question);
      this.config = {leftTime: this.time, format: 'mm:ss'};
      this.answerColors = Array(4).fill('transparent');
      console.log('--- Answer colors :  '+this.answerColors);
    },0);

  }

  quit() {
    this.router.navigateByUrl('/start/tabs/tab3');
  }

  saveAnswer(question,answer,i){
    question.enteredAnswer = answer;
    console.log('question', question);
    console.log('answer', answer);
    console.log('question.answer', question.enteredAnswer);

    console.log('(question.enteredAnswer === question.answer) : '+(question.enteredAnswer === question.answer));
    if(question.enteredAnswer === question.answer){
      this.numberOfCorrectAnswers++;
      this.answerColors[i] = '#025c25'; //correct Green Color
    }
    else {
      this.answerColors[i] = 'red'; //wrong red Color
      for (let j = 0; j < question.answers.length; j++) {
        if(question.answers[j] === question.answer){
          console.log('------------Correct answer : '+j);
          this.answerColors[j] = '#025c25'; //correct Green Color
        }
      }
    }

    this.pauseUntilNext();

    //this.resetTimer();



    // this.question = this.quiz.questions[question.id];
    // this.resetTimer();


  }

  handleTimer(e: CountdownEvent) {

    if (e.action==='start'){
      console.log('Actions', e);
      console.log('CountDown Start');
    }

    if (e.action==='done'){
      console.log('Actions', e);
      console.log('CountDown Complete');
      for (let j = 0; j < 4; j++) {
        if(this.question.answers[j] === this.question.answer){
          console.log('------------Correct answer : '+j);
          this.answerColors[j] = '#025c25'; //correct Green Color
        }
      }
      this.pauseUntilNext();

    }
  }


  update(){

    //var completeLevel = 'L'+completeLevel;
    //this.userService.updateUser(id, completeLevel, marks, currentLevel, this.passOrFail);
    if (this.passOrFail === 'pass' && this.currentLevel === this.userService.getTopLevel()) {
      this.userService.setTopLevel(this.currentLevel+1);
    }
    console.log('currLevel : '+this.userService.getCurrentLevel());
    console.log('topLevel : '+this.userService.getTopLevel());

    if (this.numberOfCorrectAnswers / this.questions.length * 100 > this.userService.getScore(this.currentLevel)) {
      this.userService.setScore(this.currentLevel,this.numberOfCorrectAnswers / this.questions.length * 100);
    }
  }

  startTimer(){
    this.config = {leftTime: this.time, format: 'mm:ss', demand: false};
    this.counter.begin();
  }


  resetTimer() {
    this.question = this.questions[this.id++];
    this.nextButton = true;
    if (!this.question){
      console.log('------------Next button :',this.nextButton);

      if (this.numberOfCorrectAnswers >= 2){
        this.passOrFail = 'pass';
      } else {
        //this.update(this.id, this.level.toString(), this.numberOfCorrectAnswers, this.currentLevel);
        console.log(' this.numberOfCorrectAnswers', this.numberOfCorrectAnswers);
      }
      this.update();
      this.showResult = true;

    }
    else {
      this.question.answers = this.shuffle([this.question.answer,this.question.option1,this.question.option2,this.question.option3]);
      this.answerColors = Array(4).fill('transparent');
      console.log('--- Answer colors :  '+this.answerColors);
      this.counter.restart();
    }
  }


  pauseUntilNext() {
    this.counter.pause();
    this.nextButton = false;
  }

  shuffle(array) {
    let currentIndex = array.length;
    let randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex !== 0) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }

    return array;
  }


}
