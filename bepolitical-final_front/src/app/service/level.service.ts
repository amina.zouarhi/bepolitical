import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LevelService {

  private questionsPerLevel: any = [
    {
      id: 1,
      question: "Que représente le 14 Juillet ?",
      answer: " C’est l’anniversaire de la prise de la bastille",
      imageUrl: "http://localhost:8080/question/image/server3.png",
      option1: " C’est la fin de la première guerre mondiale",
      option2: "C’est la fête nationale",
      option3: " C’est l’anniversaire de la prise de la bastille",
      levelStatus: "LEVEL_1"
    },
    {
      id: 2,
      question: "Que représente ?",
      answer: " C’est l’anniversaire de la prise de la bastille",
      imageUrl: "http://localhost:8080/question/image/server3.png",
      option1: " C’est la fin de la première guerre mondiale",
      option2: "C’est la fête nationale",
      option3: " C’est l’anniversaire de la prise de la bastille",
      levelStatus: "LEVEL_1"
    },
    {
      id: 6,
      question: "Que ?",
      answer: " C’est l’anniversaire de la prise de la bastille",
      imageUrl: "http://localhost:8080/question/image/server3.png",
      option1: " C’est la fin de la première guerre mondiale",
      option2: "C’est la fête nationale",
      option3: " C’est l’anniversaire de la prise de la bastille",
      levelStatus: "LEVEL_1"
    },
    {
      id: 4,
      question: "Que Juillet ?",
      answer: " C’est l’anniversaire de la prise de la bastille",
      imageUrl: "http://localhost:8080/question/image/server3.png",
      option1: " C’est la fin de la première guerre mondiale",
      option2: "C’est la fête nationale",
      option3: " C’est l’anniversaire de la prise de la bastille",
      levelStatus: "LEVEL_2"
    },
    {
      id: 3,
      question: "Que représente le 14 Juillet ?",
      answer: " C’est l’anniversaire de la prise de la bastille",
      imageUrl: "http://localhost:8080/question/image/server3.png",
      option1: " C’est la fin de la première guerre mondiale",
      option2: "C’est la fête nationale",
      option3: " C’est l’anniversaire de la prise de la bastille",
      levelStatus: "LEVEL_2"
    },
    {
      id: 4,
      question: "Que 14 Juillet ?",
      answer: " C’est l’anniversaire de la prise de la bastille",
      imageUrl: "http://localhost:8080/question/image/server3.png",
      option1: " C’est la fin de la première guerre mondiale",
      option2: "C’est la fête nationale",
      option3: " C’est l’anniversaire de la prise de la bastille",
      levelStatus: "LEVEL_2"
    }];

    /*   {id: 1, question: 'Question 1', level: 1, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep1'},
      {id: 2, question: 'Question 2', level: 1, answers: ['rep4','rep5','rep6'], corrAnswer: 'rep5'},
      {id: 3, question: 'Question 3', level: 1, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep2'},
      {id: 4, question: 'Question 4', level: 2, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep2'},
      {id: 5, question: 'Question 5', level: 2, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep2'},
      {id: 6, question: 'Question 6', level: 2, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep2'},
      {id: 7, question: 'Question 7', level: 3, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep2'},
      {id: 8, question: 'Question 8', level: 3, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep2'},
      {id: 9, question: 'Question 9', level: 3, answers: ['rep1','rep2','rep3'], corrAnswer: 'rep2'},
    ]; */

  constructor() { }

  getLevelQuestions(level) {
    const questions = [];

    let i = 1;
    this.questionsPerLevel.forEach(element => {
      if (element.levelStatus === 'LEVEL_'+level) {
        element.id = i;
        questions.push(element);
        i++;
      }
    });
    questions.forEach(element => {
      console.log('--- Question : '+element.question);
    });
    return questions;
  }

}
