import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentLevel = 0;
  private topLevel = 1;
  private scoresPerLevel = [{level: 1, score: 60},
                            {level: 2, score: 0},
                            {level: 3, score: 0},
                            {level: 4, score: 0},
                            {level: 5, score: 0}];
  private users = [{
              id: 19,
              userName: "user4",
              email: "user4@user4.com",
              password: "$2a$10$Dx.nvXs4HCheKvPnsdV2v.7J8z8Dcn2l3NOYEyQHRlNmH1moIsBF.",
              name: "user2",
              lastName: "test",
              active: true,
              score: [
                  30,
                  10,
                  20,
                  50,
                  40
              ]
            },
            {
              id: 20,
              userName: "user42",
              email: "user42@user42.com",
              password: "$2a$10$Dx.nvXs4HCheKvPnsdV2v.7J8z8Dcn2l3NOYEyQHRlNmH1moIsBF.",
              name: "user42",
              lastName: "test",
              active: true,
              score: [
                  30,
                  10,
                  20,
                  50,
                  40
              ]
            },
            {
              id: 21,
              userName: "user43",
              email: "user43@user43.com",
              password: "$2a$10$Dx.nvXs4HCheKvPnsdV2v.7J8z8Dcn2l3NOYEyQHRlNmH1moIsBF.",
              name: "user432",
              lastName: "test",
              active: true,
              score: [
                  30,
                  10,
                  20,
                  50,
                  40
              ]
            }];

  private user : any;


  constructor() { }

  getCurrentLevel() {
    return this.currentLevel;
  }

  setCurrentLevel(level) {
    this.currentLevel = level;
  }

  getTopLevel() {
    return this.topLevel;
  }

  setTopLevel(level) {
    this.topLevel = level;
  }


  getScores() {
    return this.scoresPerLevel;
  }

  getScore(level) {
    return this.scoresPerLevel[level-1].score;
  }

  setScore(level,newScore) {
    this.scoresPerLevel[level-1].score = newScore;
  }

  getUsers() {
    return this.users;
  }

  getUser(userName) {
    for (const user of this.users) {
      if (user.email === userName) {
        return user;
      }
    }
    return 'Cet utilisateur n\'existe pas!';
  }

  getCurrentUser() {
    return this.user;
  }

  setUser(userData) {
    this.user = userData;
  }

  addUser(userData) {
    this.users.push(userData);
  }

  updateUser(username,mail,password,name,lastname) {
    this.user.userName = username;
    this.user.email = mail;
    this.user.password = password;
    this.user.name = name;
    this.user.lastName = lastname;

  }


}
