import { Component, ViewEncapsulation } from '@angular/core';
import { SwiperOptions } from 'swiper';
import SwiperCore, {Pagination} from 'swiper';
import { Router } from '@angular/router';

SwiperCore.use([Pagination]);

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Tab1Page {

  config: SwiperOptions = {
    pagination: true
  }

  constructor(private routes: Router) {}

  play(){
    this.routes.navigateByUrl('/start/tabs/tab3');
  }
}
