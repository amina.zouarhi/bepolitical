import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  user:any;
  disabled = true;
  name:string;
  lastname:string;
  username:string;
  mail:string;
  password:string;


  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {
    this.user = this.userService.getCurrentUser();
    console.log('---User : '+this.user.email);

  }

  ngOnInit() {
    this.user = this.userService.getCurrentUser();
    console.log('---User : '+this.user.email);

  }

  disable(): void{


    console.log('this.name : '+this.name);
    this.userService.updateUser(this.username,this.mail,this.password,this.name,this.lastname);
    this.disabled =true;
  }

  enable(): void{
    this.name = this.user.name;
    this.lastname = this.user.lastName;
    this.mail = this.user.email;
    this.username = this.user.userName;
    this.disabled =false;
  }

}
