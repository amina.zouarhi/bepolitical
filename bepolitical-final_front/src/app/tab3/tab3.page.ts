import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  scoresPerLevel: any;
  topLevel: number;
  width: any;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {
    this.route.params.subscribe(() => {
      this.topLevel = this.userService.getTopLevel();
      this.scoresPerLevel = this.userService.getScores();
      // add loading bar
      this.width = this.scoresPerLevel.map(elem => (elem.score).toString()+'%');
      console.log('------------- newar : '+this.width);


    });
  }

  selectLevel(level){
    this.userService.setCurrentLevel(level);
    this.router.navigateByUrl('/question/'+level);

  }

}
